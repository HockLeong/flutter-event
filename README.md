# Event App
A simple event list demo app written in Flutter using GetX. It is designed using BLoC architecture design pattern.
It will get event data from [Ticket Master](https://developer.ticketmaster.com/) website.

## Feature
- Event List (Page that display all event list, allow navigate to Event Details page)
- Event Details (Page that display the details of the selected item like image, event name and date time)

## Getting Started

**Step 1:**
Clone the repo with the link below by using git:
```
git clone https://gitlab.com/HockLeong/flutter-event.git
```

**Step 2:**
Current project is using flutter **Version 3.3.6**. Ensure using the appropriate flutter version before running.

**Step 3:**
Go to project root and execute the following command in console to get the required dependencies:
```
flutter pub get 
```

**Extra(Optional)**
For those who using iOS emulator for testing, there might got chance in [simulator freeze issue](https://developer.apple.com/forums/thread/122972) when displaying TextForm due to apple side issue.
If facing this issue please don't feel is project/code error. Few steps can be done using following steps

1. Please restart your iOS simulator and see whether still facing same issue or not. If yes, please refer to next step
2. [Solution 2](https://stackoverflow.com/a/58607330)

Feel free to let me know if still facing issue in opening using simulator.

### Libraries & Tools Used
- [X] [Flutter BLoC](https://github.com/felangel/bloc/tree/master/packages/flutter_bloc)
- [x] [GetX](https://github.com/jonataslaw/getx)
- [x] [Dio](https://github.com/cfug/dio/tree/main/dio)
- [x] [Intl](https://github.com/dart-lang/intl)
- [X] [Json Annotation](https://github.com/google/json_serializable.dart/tree/master/json_annotation)

### Folder Structure
Here is the core folder structure flutter provides.
```
flutter-recipe/
|- android
|- build
|- ios
|- lib
|- linux
|- macos
|- test
|- web
|- windows
```

Here is the folder structure using in this project
```
lib/
|- blocs/
|- constants/
|- extensions/
|- model/
|- network/
|- repositories/
|- routes/
|- ui/
|- utils/
|- widgets/
|- app_provider.dart
|- main.dart
```
#### Blocs
Blocs basically handling all kinds of business logic and provides the data for a specific UI component using events and states

#### Constant
Contains constant required for the project like `colors`, `constant`, `endpoints` and `themes`

#### Extensions
Contains the extension functions of your application

#### Model
Contains the data layer of your project
Model consists of all the data classes required for the project, including requests and responses

#### Network
Consists of API client using dio to make network request

#### Repositories
Handle data operations and know where to get the data whether from local DB or API (For current project we used local DB only).
It acts as middleman between data sources

#### Routes
Contains all routes that app required for navigation purpose

#### UI
Contains all the layout required for the applications. E.g. screen

#### Utils
Contains the utilities/common functions of your application

#### Widgets
Contain all common widgets that shared across multiple screens for the application

#### App Provider
Act as dependency injection (DI) so that instance of a Bloc/Cubit can be provided to multiple widgets within a subtree

#### Main
Starting point of the application. All application level configurations are defined in this file
E.g. theme, routes, title, home screen and etc

## Test
Here is the folder structure using for unit and widget testing in this project
```
test
|- unit
|- widget
```

Go to project root and execute the following command in console to run for unit testing:
```
flutter test test/unit/event_test.dart
```

Execute the following command in console to run for widget testing:
```
flutter test test/widget/widget_test.dart
```
