import 'package:flutter/material.dart';
import 'package:flutter_ticket/constants/assets.dart';

import '../constants/constant.dart';

class ImageWidget extends StatelessWidget {
  const ImageWidget({
    Key? key,
    this.height,
    this.width,
    this.imageUrl,
  }) : super(key: key);

  final double? height;
  final double? width;
  final String? imageUrl;

  @override
  Widget build(BuildContext context) {
    return SizedBox(
      height: height ?? MediaQuery.of(context).size.height * 0.3,
      width: width ?? double.infinity,
      child: FadeInImage.assetNetwork(
        image: imageUrl ?? Constant.PLACEHOLDER_IMAGE_URL,
        placeholder: Assets.PLACEHOLDER,
        fit: BoxFit.cover,
      ),
    );
  }
}
