import 'package:flutter_ticket/model/event_model.dart';

abstract class IEventRepository {
  Future<List<EventModel>> getEvents({
    Map<String, dynamic>? queryParams,
  });

  Future<EventModel> getEventDetails(
    String eventId, {
    Map<String, dynamic>? queryParams,
  });
}
