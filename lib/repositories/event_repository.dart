import 'package:flutter_ticket/extensions/string_extension.dart';
import 'package:flutter_ticket/model/event_model.dart';
import 'package:flutter_ticket/model/generic_base_model.dart';
import 'package:flutter_ticket/network/api_client.dart';
import 'package:flutter_ticket/repositories/i_event_repository.dart';

import '../constants/endpoints.dart';
import '../model/embedded_model.dart';

const List<EventModel> mockEvents = [];

class EventRepository implements IEventRepository {
  EventRepository({
    required ApiClient apiClient,
  }) : _apiClient = apiClient;

  final ApiClient _apiClient;

  @override
  Future<List<EventModel>> getEvents({
    Map<String, dynamic>? queryParams,
  }) async {
    var response = await _apiClient.get(
      Endpoints.eventSearch,
      queryParameters: queryParams,
    );

    return GenericBaseModel<EmbeddedModel>.fromJson(
          response,
          (data) => EmbeddedModel.fromJson(data),
        ).data?.events ??
        [];
  }

  @override
  Future<EventModel> getEventDetails(
    String eventId, {
    Map<String, dynamic>? queryParams,
  }) async {
    var response = await _apiClient.get(
      Endpoints.eventDetails.interpolate(params: {'eventId': eventId}),
      queryParameters: queryParams,
    );

    return EventModel.fromJson(response);
  }
}
