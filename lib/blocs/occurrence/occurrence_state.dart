part of 'occurrence_bloc.dart';

@immutable
abstract class OccurrenceState extends Equatable {
  const OccurrenceState();
}

class OccurrenceLoading extends OccurrenceState {
  @override
  List<Object> get props => [];
}

class OccurrenceLoaded extends OccurrenceState {
  const OccurrenceLoaded({
    required this.events,
  });

  final List<EventModel> events;

  @override
  List<Object> get props => [
        events,
      ];
}

class OccurrenceError extends OccurrenceState {
  const OccurrenceError({
    this.message,
  });

  final String? message;

  @override
  List<Object> get props => [];
}
