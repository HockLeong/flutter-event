import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:meta/meta.dart';

import '../../constants/constant.dart';
import '../../model/event_model.dart';
import '../../repositories/i_event_repository.dart';

part 'occurrence_event.dart';

part 'occurrence_state.dart';

class OccurrenceBloc extends Bloc<OccurrenceEvent, OccurrenceState> {
  final IEventRepository repository;

  OccurrenceBloc(this.repository) : super(OccurrenceLoading()) {
    on<OccurrenceRequested>(_getEvents);
  }

  Future<void> _getEvents(
    OccurrenceRequested event,
    Emitter<OccurrenceState> emit,
  ) async {
    emit(OccurrenceLoading());
    try {
      Map<String, dynamic> queryParam = {
        "apikey": Constant.API_KEY,
        "size": 30,
      };

      if (event.keyword?.isNotEmpty == true) {
        queryParam["keyword"] = event.keyword;
      }

      List<EventModel> data = await repository.getEvents(
        queryParams: queryParam,
      );

      emit(OccurrenceLoaded(events: data));
    } catch (e) {
      emit(OccurrenceError(message: e.toString()));
    }
  }
}
