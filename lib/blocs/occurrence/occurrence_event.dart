part of 'occurrence_bloc.dart';

@immutable
abstract class OccurrenceEvent extends Equatable {
  const OccurrenceEvent();
}

class OccurrenceRequested extends OccurrenceEvent {
  const OccurrenceRequested({
    this.keyword,
  });

  final String? keyword;

  @override
  List<Object?> get props => [keyword];
}
