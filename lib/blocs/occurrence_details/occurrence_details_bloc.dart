import 'package:equatable/equatable.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_ticket/repositories/i_event_repository.dart';
import 'package:meta/meta.dart';

import '../../constants/constant.dart';
import '../../model/event_model.dart';

part 'occurrence_details_event.dart';

part 'occurrence_details_state.dart';

class OccurrenceDetailsBloc
    extends Bloc<OccurrenceDetailsEvent, OccurrenceDetailsState> {
  final IEventRepository repository;

  OccurrenceDetailsBloc(this.repository) : super(OccurrenceDetailsLoading()) {
    on<OccurrenceDetailsRequested>(_getEventDetails);
  }

  Future<void> _getEventDetails(
    OccurrenceDetailsRequested event,
    Emitter<OccurrenceDetailsState> emit,
  ) async {
    emit(OccurrenceDetailsLoading());
    try {
      EventModel data = await repository.getEventDetails(
        event.eventId,
        queryParams: {
          "apikey": Constant.API_KEY,
        },
      );
      emit(OccurrenceDetailsLoaded(event: data));
    } catch (e) {
      emit(OccurrenceDetailsError(message: e.toString()));
    }
  }
}
