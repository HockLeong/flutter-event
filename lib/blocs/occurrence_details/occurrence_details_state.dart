part of 'occurrence_details_bloc.dart';

@immutable
abstract class OccurrenceDetailsState extends Equatable {
  const OccurrenceDetailsState();
}

class OccurrenceDetailsLoading extends OccurrenceDetailsState {
  @override
  List<Object> get props => [];
}

class OccurrenceDetailsLoaded extends OccurrenceDetailsState {
  const OccurrenceDetailsLoaded({
    required this.event,
  });

  final EventModel event;

  @override
  List<Object> get props => [event];
}

class OccurrenceDetailsError extends OccurrenceDetailsState {
  const OccurrenceDetailsError({
    this.message,
  });

  final String? message;

  @override
  List<Object> get props => [];
}
