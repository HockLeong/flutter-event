part of 'occurrence_details_bloc.dart';

@immutable
abstract class OccurrenceDetailsEvent extends Equatable {
  const OccurrenceDetailsEvent();
}

class OccurrenceDetailsRequested extends OccurrenceDetailsEvent {
  final String eventId;

  const OccurrenceDetailsRequested(this.eventId);

  @override
  List<Object> get props => [eventId];
}
