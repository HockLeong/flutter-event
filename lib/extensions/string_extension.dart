extension StringExtension on String {
  String interpolate({Map<String, dynamic> params = const {}}) {
    if (isEmpty) return this;

    String result = this;
    for (var key in params.keys) {
      result = result.replaceAll('{{$key}}', params[key].toString());
    }

    return result;
  }

  bool parseBool() {
    return toLowerCase() == 'true';
  }
}
