import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_ticket/blocs/occurrence/occurrence_bloc.dart';
import 'package:flutter_ticket/network/api_client.dart';
import 'package:flutter_ticket/repositories/event_repository.dart';

import 'blocs/occurrence_details/occurrence_details_bloc.dart';

class AppProvider extends StatefulWidget {
  const AppProvider({
    Key? key,
    required this.child,
  }) : super(key: key);

  final Widget child;

  @override
  State<AppProvider> createState() => _AppProviderState();
}

class _AppProviderState extends State<AppProvider> {
  EventRepository? eventRepository;
  ApiClient? apiClient;

  @override
  void initState() {
    apiClient = ApiClient();

    eventRepository = EventRepository(
      apiClient: apiClient!,
    );

    super.initState();
  }

  @override
  Widget build(BuildContext context) {
    return MultiRepositoryProvider(
      providers: [
        RepositoryProvider<EventRepository>(
          lazy: false,
          create: (context) => eventRepository!,
        ),
      ],
      child: MultiBlocProvider(
        providers: [
          BlocProvider<OccurrenceBloc>(
            create: (BuildContext context) => OccurrenceBloc(
              eventRepository!,
            ),
            lazy: false,
          ),
          BlocProvider<OccurrenceDetailsBloc>(
            create: (BuildContext context) => OccurrenceDetailsBloc(
              eventRepository!,
            ),
            lazy: false,
          ),
        ],
        child: widget.child,
      ),
    );
  }
}
