import 'dart:async';

import 'package:flutter/foundation.dart';

class Debouncer {
  final int milliseconds;
  final VoidCallback? action;
  Timer? _timer;

  Debouncer({
    this.milliseconds = 500,
    this.action,
  });

  run(VoidCallback action) {
    if (_timer != null) {
      _timer?.cancel();
    }

    _timer = Timer(
      Duration(milliseconds: milliseconds),
      action,
    );
  }

  dispose() {
    _timer?.cancel();
  }
}
