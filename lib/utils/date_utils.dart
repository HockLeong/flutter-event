import 'package:intl/intl.dart';

class DateUtil {
  static const String UTC_FORMAT = "yyyy-MM-ddTHH:mm:ssZ";
  static const String DATE_TIME_FORMAT = "dd MMM yyyy hh:mm";

  static String formatUtcTime(String date) {
    if (date.isEmpty) {
      return "";
    }

    var dateValue = DateFormat(UTC_FORMAT).parseUTC(date).toLocal();
    return DateFormat(DATE_TIME_FORMAT).format(dateValue);
  }
}
