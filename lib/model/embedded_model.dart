import 'package:flutter_ticket/model/event_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'embedded_model.g.dart';

@JsonSerializable()
class EmbeddedModel {
  @JsonKey(name: 'events')
  final List<EventModel>? events;

  EmbeddedModel({
    this.events,
  });

  factory EmbeddedModel.fromJson(Map<String, dynamic> json) =>
      _$EmbeddedModelFromJson(json);

  Map<String, dynamic> toJson() => _$EmbeddedModelToJson(this);
}
