// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'embedded_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EmbeddedModel _$EmbeddedModelFromJson(Map<String, dynamic> json) =>
    EmbeddedModel(
      events: (json['events'] as List<dynamic>?)
          ?.map((e) => EventModel.fromJson(e as Map<String, dynamic>))
          .toList(),
    );

Map<String, dynamic> _$EmbeddedModelToJson(EmbeddedModel instance) =>
    <String, dynamic>{
      'events': instance.events,
    };
