// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_date_start_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventDateStartModel _$EventDateStartModelFromJson(Map<String, dynamic> json) =>
    EventDateStartModel(
      localDate: json['localDate'] as String?,
      localTime: json['type'] as String?,
      dateTime: json['dateTime'] as String?,
    );

Map<String, dynamic> _$EventDateStartModelToJson(
        EventDateStartModel instance) =>
    <String, dynamic>{
      'localDate': instance.localDate,
      'type': instance.localTime,
      'dateTime': instance.dateTime,
    };
