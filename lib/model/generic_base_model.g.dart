// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'generic_base_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

GenericBaseModel<T> _$GenericBaseModelFromJson<T>(
  Map<String, dynamic> json,
  T Function(Object? json) fromJsonT,
) =>
    GenericBaseModel<T>(
      status: $enumDecodeNullable(_$StatusEnumMap, json['status']),
      data: _$nullableGenericFromJson(json['_embedded'], fromJsonT),
      page: json['page'] == null
          ? null
          : PaginationModel.fromJson(json['page'] as Map<String, dynamic>),
    );

Map<String, dynamic> _$GenericBaseModelToJson<T>(
  GenericBaseModel<T> instance,
  Object? Function(T value) toJsonT,
) =>
    <String, dynamic>{
      'status': _$StatusEnumMap[instance.status],
      'page': instance.page,
      '_embedded': _$nullableGenericToJson(instance.data, toJsonT),
    };

const _$StatusEnumMap = {
  Status.LOADING: 'LOADING',
  Status.COMPLETED: 'COMPLETED',
  Status.ERROR: 'ERROR',
};

T? _$nullableGenericFromJson<T>(
  Object? input,
  T Function(Object? json) fromJson,
) =>
    input == null ? null : fromJson(input);

Object? _$nullableGenericToJson<T>(
  T? input,
  Object? Function(T value) toJson,
) =>
    input == null ? null : toJson(input);
