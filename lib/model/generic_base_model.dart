import 'package:json_annotation/json_annotation.dart';

import 'pagination_model.dart';

part 'generic_base_model.g.dart';

@JsonSerializable(genericArgumentFactories: true)
class GenericBaseModel<T> {
  final Status? status;
  @JsonKey(name: 'page')
  final PaginationModel? page;
  @JsonKey(name: '_embedded')
  final T? data;

  GenericBaseModel({
    this.status,
    this.data,
    this.page,
  });

  GenericBaseModel.loading({
    this.data,
    this.page,
  }) : status = Status.LOADING;

  GenericBaseModel.completed({
    this.data,
    this.page,
  }) : status = Status.COMPLETED;

  GenericBaseModel.error({
    this.data,
    this.page,
  }) : status = Status.ERROR;

  factory GenericBaseModel.fromJson(
    Map<String, dynamic> json,
    T Function(dynamic json) fromJsonT,
  ) =>
      _$GenericBaseModelFromJson(json, fromJsonT);

  Map<String, dynamic> toJson(Object Function(T value) toJsonT) => _$GenericBaseModelToJson(this, toJsonT);
}

enum Status { LOADING, COMPLETED, ERROR }
