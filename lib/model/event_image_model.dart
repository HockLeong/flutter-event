import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

part 'event_image_model.g.dart';

@JsonSerializable()
class EventImageModel extends Equatable {
  @JsonKey(name: 'ratio')
  final String? ratio;
  @JsonKey(name: 'url')
  final String? url;
  @JsonKey(name: 'width')
  final int? width;
  @JsonKey(name: 'height')
  final int? height;
  @JsonKey(name: 'fallback')
  final bool? fallback;

  const EventImageModel({
    this.ratio,
    this.url,
    this.width,
    this.height,
    this.fallback,
  });

  factory EventImageModel.fromJson(Map<String, dynamic> json) =>
      _$EventImageModelFromJson(json);

  Map<String, dynamic> toJson() => _$EventImageModelToJson(this);

  @override
  List<Object?> get props => [
        ratio,
        url,
        width,
        height,
        fallback,
      ];
}
