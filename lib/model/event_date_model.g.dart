// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_date_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventDateModel _$EventDateModelFromJson(Map<String, dynamic> json) =>
    EventDateModel(
      start: json['start'] == null
          ? null
          : EventDateStartModel.fromJson(json['start'] as Map<String, dynamic>),
      timezone: json['timezone'] as String?,
      spanMultipleDays: json['spanMultipleDays'] as bool?,
    );

Map<String, dynamic> _$EventDateModelToJson(EventDateModel instance) =>
    <String, dynamic>{
      'start': instance.start,
      'timezone': instance.timezone,
      'spanMultipleDays': instance.spanMultipleDays,
    };
