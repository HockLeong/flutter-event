import 'package:json_annotation/json_annotation.dart';

part 'pagination_model.g.dart';

@JsonSerializable()
class PaginationModel {
  @JsonKey(name: 'size')
  final int? size;
  @JsonKey(name: 'totalElements')
  final int? totalElements;
  @JsonKey(name: 'totalPages')
  final int? previousCursor;
  @JsonKey(name: 'number')
  final int? number;

  PaginationModel({
    this.size,
    this.totalElements,
    this.previousCursor,
    this.number,
  });

  factory PaginationModel.fromJson(Map<String, dynamic> json) =>
      _$PaginationModelFromJson(json);

  Map<String, dynamic> toJson() => _$PaginationModelToJson(this);
}
