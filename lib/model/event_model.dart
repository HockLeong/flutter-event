import 'package:equatable/equatable.dart';
import 'package:flutter_ticket/model/event_date_model.dart';
import 'package:json_annotation/json_annotation.dart';

import 'event_image_model.dart';

part 'event_model.g.dart';

@JsonSerializable()
class EventModel extends Equatable {
  @JsonKey(name: 'name')
  final String? name;
  @JsonKey(name: 'type')
  final String? type;
  @JsonKey(name: 'id')
  final String? id;
  @JsonKey(name: 'images')
  final List<EventImageModel>? images;
  @JsonKey(name: 'dates')
  final EventDateModel? dates;

  const EventModel({
    this.name,
    this.type,
    this.id,
    this.images,
    this.dates,
  });

  factory EventModel.fromJson(Map<String, dynamic> json) =>
      _$EventModelFromJson(json);

  Map<String, dynamic> toJson() => _$EventModelToJson(this);

  @override
  List<Object?> get props => [
        name,
        type,
        id,
        images,
        dates,
      ];
}
