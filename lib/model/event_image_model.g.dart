// GENERATED CODE - DO NOT MODIFY BY HAND

part of 'event_image_model.dart';

// **************************************************************************
// JsonSerializableGenerator
// **************************************************************************

EventImageModel _$EventImageModelFromJson(Map<String, dynamic> json) =>
    EventImageModel(
      ratio: json['ratio'] as String?,
      url: json['url'] as String?,
      width: json['width'] as int?,
      height: json['height'] as int?,
      fallback: json['fallback'] as bool?,
    );

Map<String, dynamic> _$EventImageModelToJson(EventImageModel instance) =>
    <String, dynamic>{
      'ratio': instance.ratio,
      'url': instance.url,
      'width': instance.width,
      'height': instance.height,
      'fallback': instance.fallback,
    };
