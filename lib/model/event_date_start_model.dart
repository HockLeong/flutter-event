import 'package:equatable/equatable.dart';
import 'package:json_annotation/json_annotation.dart';

import '../utils/date_utils.dart';

part 'event_date_start_model.g.dart';

@JsonSerializable()
class EventDateStartModel extends Equatable {
  @JsonKey(name: 'localDate')
  final String? localDate;
  @JsonKey(name: 'type')
  final String? localTime;
  @JsonKey(name: 'dateTime')
  final String? dateTime;

  const EventDateStartModel({
    this.localDate,
    this.localTime,
    this.dateTime,
  });

  String getDateTime() {
    String eventDateTime = "";
    if (dateTime?.isNotEmpty == true) {
      eventDateTime = DateUtil.formatUtcTime(dateTime!);
    } else {
      if (localDate?.isNotEmpty == true) {
        eventDateTime = '$localDate ';
      }

      if (localTime?.isNotEmpty == true) {
        eventDateTime += '$localTime';
      }
    }

    return eventDateTime;
  }

  factory EventDateStartModel.fromJson(Map<String, dynamic> json) =>
      _$EventDateStartModelFromJson(json);

  Map<String, dynamic> toJson() => _$EventDateStartModelToJson(this);

  @override
  List<Object?> get props => [
        localDate,
        localTime,
        dateTime,
      ];
}
