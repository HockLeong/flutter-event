import 'package:equatable/equatable.dart';
import 'package:flutter_ticket/model/event_date_start_model.dart';
import 'package:json_annotation/json_annotation.dart';

part 'event_date_model.g.dart';

@JsonSerializable()
class EventDateModel extends Equatable {
  @JsonKey(name: 'start')
  final EventDateStartModel? start;
  @JsonKey(name: 'timezone')
  final String? timezone;
  @JsonKey(name: 'spanMultipleDays')
  final bool? spanMultipleDays;

  const EventDateModel({
    this.start,
    this.timezone,
    this.spanMultipleDays,
  });

  factory EventDateModel.fromJson(Map<String, dynamic> json) =>
      _$EventDateModelFromJson(json);

  Map<String, dynamic> toJson() => _$EventDateModelToJson(this);

  @override
  List<Object?> get props => [
        start,
        timezone,
        spanMultipleDays,
      ];
}
