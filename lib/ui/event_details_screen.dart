import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_ticket/model/event_model.dart';
import 'package:get/get.dart';

import '../blocs/occurrence_details/occurrence_details_bloc.dart';
import '../constants/colors.dart';
import '../widgets/image_widget.dart';

class EventDetailsScreen extends StatefulWidget {
  const EventDetailsScreen({
    Key? key,
    this.eventId,
  }) : super(key: key);

  final String? eventId;

  @override
  State<EventDetailsScreen> createState() => _EventDetailsScreenState();
}

class _EventDetailsScreenState extends State<EventDetailsScreen> {
  @override
  void initState() {
    super.initState();
    _init();
  }

  void _init() async {
    // Load events
    context
        .read<OccurrenceDetailsBloc>()
        .add(OccurrenceDetailsRequested(widget.eventId ?? ""));
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: AppColors.backgroundColor,
      appBar: AppBar(
        title: const Text(
          "Event Details",
          style: TextStyle(color: Colors.black),
        ),
      ),
      body: BlocConsumer<OccurrenceDetailsBloc, OccurrenceDetailsState>(
          listener: (context, state) {
        if (state is OccurrenceDetailsError) {
          WidgetsBinding.instance.addPostFrameCallback((_) {
            Get.snackbar('Error', state.message!);
          });
        }
      }, builder: (context, state) {
        if (state is OccurrenceDetailsLoading) {
          return const Center(
            child: CircularProgressIndicator(),
          );
        }

        if (state is OccurrenceDetailsLoaded) {
          return _eventDetails(state.event);
        }

        return const SizedBox.shrink();
      }),
    );
  }

  Widget _eventDetails(EventModel event) {
    return SingleChildScrollView(
      child: Column(
        children: [
          ImageWidget(
            height: MediaQuery.of(context).size.height * 0.3,
            width: double.infinity,
            imageUrl: event.images?[0].url,
          ),
          Container(
            width: double.infinity,
            padding: const EdgeInsets.all(16.0),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Text(
                  event.name ?? '',
                  style: const TextStyle(
                    color: AppColors.primaryDarkTextColor,
                    fontWeight: FontWeight.bold,
                    fontSize: 18.0,
                  ),
                ),
                const SizedBox(height: 8.0),
                Text(
                  event.dates?.start?.getDateTime() ?? "",
                  style: const TextStyle(
                    color: AppColors.secondaryDarkTextColor,
                    fontWeight: FontWeight.w500,
                    fontSize: 12,
                  ),
                ),
              ],
            ),
          ),
        ],
      ),
    );
  }
}
