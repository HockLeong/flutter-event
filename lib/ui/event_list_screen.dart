import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_ticket/blocs/occurrence/occurrence_bloc.dart';
import 'package:get/get.dart';

import '../constants/colors.dart';
import '../model/event_model.dart';
import '../utils/debouncer.dart';
import '../widgets/image_widget.dart';
import 'event_details_screen.dart';

class EventListScreen extends StatefulWidget {
  const EventListScreen({Key? key}) : super(key: key);

  @override
  State<EventListScreen> createState() => _EventListScreenState();
}

class _EventListScreenState extends State<EventListScreen> {
  final Debouncer _deBouncer = Debouncer(milliseconds: 1000);
  final TextEditingController _searchController = TextEditingController();

  @override
  void initState() {
    super.initState();
    _init();
  }

  @override
  void dispose() {
    _deBouncer.dispose();
    _searchController.dispose();
    super.dispose();
  }

  void _init() async {
    // Load events
    loadEvent();
  }

  @override
  Widget build(BuildContext context) {
    return Scaffold(
      appBar: AppBar(
        title: const Text(
          "Events",
          style: TextStyle(color: Colors.black),
        ),
        actions: [
          IconButton(
            onPressed: () {
              // Reset any filter and load event
              _searchController.clear();
              loadEvent();
            },
            icon: const Icon(
              Icons.refresh,
              color: Colors.white,
            ),
          ),
        ],
      ),
      body: Column(
        mainAxisAlignment: MainAxisAlignment.center,
        children: <Widget>[
          _searchEventWidget(),
          Expanded(
            child: BlocConsumer<OccurrenceBloc, OccurrenceState>(
              listener: (context, state) {
                if (state is OccurrenceError) {
                  WidgetsBinding.instance.addPostFrameCallback((_) {
                    Get.snackbar('Error', state.message!);
                  });
                }
              },
              builder: (context, state) {
                if (state is OccurrenceLoading) {
                  return const Center(
                    child: CircularProgressIndicator(),
                  );
                }

                if (state is OccurrenceLoaded) {
                  if (state.events.isEmpty) {
                    return _emptyWidget();
                  }

                  return _eventListWidget(state.events);
                }

                return const SizedBox.shrink();
              },
            ),
          ),
        ],
      ),
    );
  }

  Widget _searchEventWidget() {
    return Container(
      padding: const EdgeInsets.all(8.0),
      color: Colors.white,
      child: TextField(
        controller: _searchController,
        onChanged: (value) {
          // Prevent searching if keyword is empty or less than 4 characters
          if (value.isNotEmpty && value.length > 3) {
            _deBouncer.run(() {
              loadEvent(keyword: value.trim());
            });
          }
        },
        keyboardType: TextInputType.text,
        decoration: const InputDecoration(
          hintText: "Please enter event name",
          fillColor: Colors.white,
          focusedBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: AppColors.primaryColor,
              width: 1.0,
            ),
          ),
          enabledBorder: OutlineInputBorder(
            borderSide: BorderSide(
              color: Colors.grey,
              width: 1.0,
            ),
          ),
        ),
      ),
    );
  }

  Widget _eventListWidget(List<EventModel> items) {
    return ListView.builder(
      itemCount: items.length,
      padding: const EdgeInsets.only(
        left: 16.0,
        right: 16.0,
        bottom: 16.0,
      ),
      shrinkWrap: true,
      primary: false,
      itemBuilder: (BuildContext context, int index) {
        EventModel item = items[index];

        return Card(
          color: Colors.white,
          elevation: 4.0,
          margin: const EdgeInsets.only(top: 16.0),
          shape: RoundedRectangleBorder(
            borderRadius: BorderRadius.circular(16.0),
          ),
          child: InkWell(
            onTap: () => Get.to(
              () => EventDetailsScreen(eventId: item.id),
            ),
            child: Column(
              crossAxisAlignment: CrossAxisAlignment.start,
              children: [
                Column(
                  crossAxisAlignment: CrossAxisAlignment.start,
                  children: [
                    ClipRRect(
                      borderRadius: BorderRadius.circular(16.0),
                      child: ImageWidget(
                        width: double.infinity,
                        height: 150.0,
                        imageUrl: item.images?[0].url,
                      ),
                    ),
                    Container(
                      padding: const EdgeInsets.all(16.0),
                      child: Column(
                        crossAxisAlignment: CrossAxisAlignment.start,
                        children: [
                          Text(
                            item.name ?? "",
                            style: const TextStyle(
                              color: AppColors.primaryDarkTextColor,
                              fontWeight: FontWeight.bold,
                              fontSize: 16,
                            ),
                          ),
                          const SizedBox(height: 8.0),
                          Text(
                            item.dates?.start?.getDateTime() ?? "",
                            style: const TextStyle(
                              color: AppColors.secondaryDarkTextColor,
                              fontWeight: FontWeight.w500,
                              fontSize: 12,
                            ),
                          ),
                        ],
                      ),
                    ),
                  ],
                ),
              ],
            ),
          ),
        );
      },
    );
  }

  Widget _emptyWidget() {
    return const Center(
      child: Text('No result found'),
    );
  }

  void loadEvent({
    String? keyword,
  }) {
    context.read<OccurrenceBloc>().add(OccurrenceRequested(keyword: keyword));
  }
}
