import 'package:flutter/material.dart';
import 'package:flutter_ticket/ui/event_list_screen.dart';
import 'package:get/get.dart';

import 'app_provider.dart';
import 'constants/themes.dart';
import 'routes/app_router.dart';

void main() {
  runApp(
    AppProvider(
      child: GetMaterialApp(
        title: 'EventApp',
        getPages: AppRouter.routes,
        theme: AppTheme.buildLightTheme(),
        home: const EventListScreen(),
      ),
    ),
  );
}
