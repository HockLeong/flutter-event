import 'package:flutter_ticket/ui/event_details_screen.dart';
import 'package:flutter_ticket/ui/event_list_screen.dart';
import 'package:get/get.dart';

class AppRouter {
  static final routes = [
    GetPage(
      name: '/tickets',
      page: () => const EventListScreen(),
      children: [
        GetPage(
          name: '/details',
          page: () => const EventDetailsScreen(),
        ),
      ],
    ),
  ];
}
