class Endpoints {
  Endpoints._();

  static const String _discovery = "discovery";
  static const String _version = 'v2';

  static const String _events = "events";

  // Events
  static const String eventSearch = '$_discovery/$_version/$_events';
  static const String eventDetails =
      '$_discovery/$_version/$_events/{{eventId}}';
}
