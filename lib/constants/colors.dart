import 'package:flutter/material.dart';

class AppColors {
  AppColors._(); // this basically makes it so you can't instantiate this class

  static const Color primaryColor = Color(0xFFFFBE24);
  static const Color secondaryColor = Color(0xFFFFC529);
  static const Color backgroundColor = Color(0xFFF2F2F2);
  static const Color cardStatusColor = Color(0xFFE8E4D0);

  static const Color primaryDarkTextColor = Color(0xFF1A1D26);
  static const Color secondaryDarkTextColor = Color(0xFF515154);
  static const Color primaryLightTextColor = Color(0xFF9796A1);

  static const themeGray = Color(0xFF7D8184);
  static const themeLightGray = Color(0xFFF8F8F8);
  static const themeBlue = Color(0xFF334856);
  static const shadowColor = Color(0x24000000);

  static final ColorScheme colorScheme = const ColorScheme.light().copyWith(
    primary: primaryColor,
    secondary: secondaryColor,
  );
}
