import 'package:flutter/material.dart';
import 'package:flutter_bloc/flutter_bloc.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_ticket/blocs/occurrence/occurrence_bloc.dart';
import 'package:flutter_ticket/blocs/occurrence_details/occurrence_details_bloc.dart';
import 'package:flutter_ticket/ui/event_details_screen.dart';
import 'package:flutter_ticket/ui/event_list_screen.dart';
import 'package:get/get.dart';
import 'package:network_image_mock/network_image_mock.dart';

import '../unit/mock_event_repository.dart';

void main() {
  group('Widget test', () {
    late MockEventRepository mockEventRepository;

    setUp(() {
      mockEventRepository = MockEventRepository();
    });

    testWidgets('Event list screen test', (WidgetTester tester) async {
      OccurrenceBloc occurrenceBloc = OccurrenceBloc(mockEventRepository);

      mockNetworkImagesFor(() async {
        await tester.pumpWidget(MultiRepositoryProvider(
          providers: [
            RepositoryProvider<MockEventRepository>(
              lazy: false,
              create: (context) => mockEventRepository,
            ),
          ],
          child: MultiBlocProvider(
            providers: [
              BlocProvider<OccurrenceBloc>(
                create: (BuildContext context) => occurrenceBloc,
                lazy: false,
              ),
            ],
            child: const GetMaterialApp(home: EventListScreen()),
          ),
        ));

        // Give tester enough time to process all the async calls
        await tester.pumpAndSettle(const Duration(seconds: 3));

        // Expect event list loaded
        expect(find.byType(ListView), findsOneWidget);

        // Look for search text field
        var searchTextField = find.byType(TextField);
        expect(searchTextField, findsOneWidget);

        // Try to enter valid event name and search
        await tester.enterText(searchTextField, 'Event One');
        await tester.pumpAndSettle(const Duration(seconds: 3));
        expect(find.byType(ListView), findsOneWidget);

        // Try to enter invalid event name not within event list and search
        await tester.enterText(searchTextField, 'Event Eleven');
        await tester.pumpAndSettle(const Duration(seconds: 3));
        expect(find.text('No result found'), findsOneWidget);
      });
    });

    testWidgets('Event details screen test', (WidgetTester tester) async {
      OccurrenceDetailsBloc occurrenceDetailsBloc =
          OccurrenceDetailsBloc(mockEventRepository);

      mockNetworkImagesFor(() async {
        await tester.pumpWidget(MultiRepositoryProvider(
          providers: [
            RepositoryProvider<MockEventRepository>(
              lazy: false,
              create: (context) => mockEventRepository,
            ),
          ],
          child: MultiBlocProvider(
            providers: [
              BlocProvider<OccurrenceDetailsBloc>(
                create: (BuildContext context) => occurrenceDetailsBloc,
                lazy: false,
              ),
            ],
            child: const GetMaterialApp(
              home: EventDetailsScreen(
                eventId: "1",
              ),
            ),
          ),
        ));

        // Give tester enough time to process all the async calls
        await tester.pumpAndSettle(const Duration(seconds: 3));

        // Expect event details loaded
        expect(find.text('Event One'), findsOneWidget);
        expect(find.text('2023-02-26 12:00:00'), findsOneWidget);
      });
    });
  });
}
