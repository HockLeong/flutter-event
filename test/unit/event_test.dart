import 'package:bloc_test/bloc_test.dart';
import 'package:equatable/equatable.dart';
import 'package:flutter/widgets.dart';
import 'package:flutter_test/flutter_test.dart';
import 'package:flutter_ticket/blocs/occurrence/occurrence_bloc.dart';
import 'package:flutter_ticket/blocs/occurrence_details/occurrence_details_bloc.dart';
import 'package:flutter_ticket/model/event_date_model.dart';
import 'package:flutter_ticket/model/event_date_start_model.dart';
import 'package:flutter_ticket/model/event_model.dart';

import 'mock_event_repository.dart';

void main() async {
  WidgetsFlutterBinding.ensureInitialized();

  group('EventBloc test', () {
    late OccurrenceBloc occurrenceBloc;
    late OccurrenceDetailsBloc occurrenceDetailsBloc;
    late MockEventRepository mockEventRepository;

    setUp(() {
      EquatableConfig.stringify = true;
      mockEventRepository = MockEventRepository();
      occurrenceBloc = OccurrenceBloc(mockEventRepository);
      occurrenceDetailsBloc = OccurrenceDetailsBloc(mockEventRepository);
    });

    blocTest<OccurrenceBloc, OccurrenceState>(
      'emits [OccurrenceLoading, OccurrenceLoaded] states for successful events loads',
      build: () => occurrenceBloc,
      act: (bloc) => bloc.add(const OccurrenceRequested()),
      wait: const Duration(seconds: 2),
      expect: () => [
        OccurrenceLoading(),
        OccurrenceLoaded(events: MockEventRepository.mockEvents),
      ],
    );

    blocTest<OccurrenceDetailsBloc, OccurrenceDetailsState>(
      'emits [OccurrenceDetailsLoading, OccurrenceDetailsLoaded] with correct occurrence details',
      build: () => occurrenceDetailsBloc,
      act: (bloc) => bloc.add(const OccurrenceDetailsRequested("1")),
      wait: const Duration(seconds: 2),
      expect: () => [
        OccurrenceDetailsLoading(),
        const OccurrenceDetailsLoaded(
          event: EventModel(
            id: "1",
            name: "Event One",
            type: "event",
            dates: EventDateModel(
              start: EventDateStartModel(
                localDate: "2023-02-26",
                localTime: "12:00:00",
              ),
            ),
          ),
        ),
      ],
    );

    tearDown(() {
      occurrenceBloc.close();
    });
  });
}
