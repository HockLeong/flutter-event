import 'package:flutter_ticket/model/event_date_model.dart';
import 'package:flutter_ticket/model/event_date_start_model.dart';
import 'package:flutter_ticket/model/event_model.dart';
import 'package:flutter_ticket/repositories/i_event_repository.dart';

class MockEventRepository implements IEventRepository {
  static List<EventModel> mockEvents = [
    EventModel(
      id: "1",
      name: "Event One",
      type: "event",
      dates: EventDateModel(
        start: EventDateStartModel(
          localDate: "2023-02-26",
          localTime: "12:00:00",
        ),
      ),
    ),
    EventModel(
      id: "2",
      name: "Event Two",
      type: "event",
      dates: EventDateModel(
        start: EventDateStartModel(
          localDate: "2023-07-12",
          localTime: "18:00:00",
        ),
      ),
    ),
    EventModel(
      id: "3",
      name: "Event Three",
      type: "event",
      dates: EventDateModel(
        start: EventDateStartModel(
          localDate: "2023-05-10",
          localTime: "14:00:00",
          dateTime: "2023-05-10T18:00:00Z",
        ),
      ),
    ),
  ];

  @override
  Future<List<EventModel>> getEvents({
    Map<String, dynamic>? queryParams,
  }) async {
    List<EventModel>? filterEvents;
    if (queryParams?.isNotEmpty == true &&
        queryParams!.containsKey("keyword")) {
      filterEvents = [];
      String keyword = queryParams["keyword"];
      filterEvents = mockEvents
          .where(
            (element) => element.name == keyword,
          )
          .toList();
    }

    return filterEvents ?? mockEvents;
  }

  @override
  Future<EventModel> getEventDetails(
    String eventId, {
    Map<String, dynamic>? queryParams,
  }) async {
    await Future.delayed(const Duration(seconds: 2));

    EventModel eventDetails =
        mockEvents.where((event) => event.id == eventId).first;

    return eventDetails;
  }
}
